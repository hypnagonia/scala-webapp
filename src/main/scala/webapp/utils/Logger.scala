package webapp.utils

import com.typesafe.scalalogging._
import org.slf4j.LoggerFactory

trait AppLogger {
  val log: Logger = Logger(LoggerFactory.getLogger(this.getClass()))
}
package webapp.model

case class Draft(
                  title: String,
                  text: String
                )
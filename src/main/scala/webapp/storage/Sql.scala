package webapp.storage

import doobie._
import doobie.implicits._

import webapp.model._

object Sql {
  def getList(): Query0[Int] = {
    sql"""
    select height from blocks limit 5
  """.query[Int]
  }
}
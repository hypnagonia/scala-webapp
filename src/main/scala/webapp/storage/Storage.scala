package webapp.storage

import doobie._
import doobie.implicits._

import doobie.hikari._
import doobie.hikari.implicits._

import monix.eval.Task

import webapp.utils.AppLogger

class Storage(
               private val host: String,
               private val port: Int,
               private val databaseName: String,
               private val user: String,
               private val password: String
             ) extends AppLogger {
  private val transactor: Task[HikariTransactor[Task]] =
    HikariTransactor.newHikariTransactor[Task](
      "org.postgresql.Driver",
      s"jdbc:postgresql://$host:$port/$databaseName",
      user,
      password
    ).memoize

  def query[T](query: ConnectionIO[T]): Task[T] =
    for {
      xa <- transactor
      result <- query.transact(xa)
    } yield result

  def update(query: Update0): Task[Int] =
    for {
      xa <- transactor
      result <- query.run.transact(xa)
    } yield result

  def getList(): Task[List[Int]] = query(Sql.getList().to[List])

  def close(): Unit = {
    // close connect
  }

  def init(): Unit = {
    // migrate
  }
}
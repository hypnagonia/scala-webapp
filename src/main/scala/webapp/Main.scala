package webapp

import com.typesafe.config.ConfigFactory
import java.io.File

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import monix.execution.Scheduler

import scala.io.StdIn
import webapp.storage._
import webapp.utils.AppLogger
import java.lang.Runtime.getRuntime

object Main extends App with AppLogger {
  val maybeConfigFile = for {
    maybeFilename <- args.headOption
    file = new File(maybeFilename)
    if file.exists
  } yield file

  val config = maybeConfigFile match {
    case Some(file) => {
      log.info(s"Using $file config")
      ConfigFactory.parseFile(file).withFallback(ConfigFactory.load())
    }
    case None => {
      log.warn("No config file specified. Using default settings")
      ConfigFactory.load()
    }
  }

  implicit val as = ActorSystem()
  implicit val ec = as.dispatcher
  implicit val m = ActorMaterializer()

  val availableProcessors = getRuntime.availableProcessors

  val monixSheduler = Scheduler.cached("monixSheduler", 1, availableProcessors * 2)

  val storage = new Storage(
    config.getString("db.host"),
    config.getInt("db.port"),
    config.getString("db.db"),
    config.getString("db.user"),
    config.getString("db.password")
  )

  val server = new HttpApi(storage, monixSheduler)

  val host = config.getString("app.host")
  val port = config.getInt("app.port")

  log.info(s"Starting web server at $host:$port")
  val serverFuture = server.run(host, port)

  StdIn.readLine("Press ENTER to exit")

  serverFuture
    .flatMap(_.unbind())
    .onComplete(_ => as.terminate())
}
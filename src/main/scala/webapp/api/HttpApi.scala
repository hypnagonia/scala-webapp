package webapp

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import akka.http.scaladsl.model._

import scala.concurrent.{ExecutionContextExecutor}

import webapp.model._
import webapp.storage._
import webapp.utils.AppLogger
import monix.execution.schedulers.SchedulerService

class HttpApi(
               s: Storage,
               monixSheduler: SchedulerService)
             (
               implicit actorSystem: ActorSystem,
               materializer: ActorMaterializer,
               executionContext: ExecutionContextExecutor
             )
  extends AppLogger {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
  import io.circe.generic.auto._


  private val route = pathSingleSlash {
    complete(s.getList().runToFuture(monixSheduler))
  }

  def run(host: String, port: Int) = Http().bindAndHandle(route, host, port)
}